# Load Files #
. ~/bin/bash/aliases
. ~/bin/bash/env
. ~/bin/bash/config

# PS1='${debian_chroot:+($debian_chroot)}\[\033[01;32m\]\u@\h\[\033[00m\]:\[\033[01;34m\]\w\[\033[00m\]\$ '

if ! shopt -oq posix; then
  if [ -f /usr/share/bash-completion/bash_completion ]; then
    . /usr/share/bash-completion/bash_completion
  elif [ -f /etc/bash_completion ]; then
    . /etc/bash_completion
  fi
fi

# X Server Configuration
export DISPLAY=localhost:0.0

(! /mnt/c/Windows/System32/tasklist.exe /nh /fi "imagename eq vcxsrv.exe" | grep -q "vcxsrv.exe" && /mnt/c/Windows/System32/cmd.exe /C start /b "" "C:\Program Files\Xming\Xming.exe" :0 -dpi 200 -ac -terminate -lesspointer -multiwindow -clipboard -wgl &>/dev/null &)

# Load NVM.
export NVM_DIR="/home/jessica/var/nvm"
[ -s "$NVM_DIR/nvm.sh" ] && . "$NVM_DIR/nvm.sh"

# added by Anaconda3 4.2.0 installer
export PATH="/home/jessica/var/anaconda3/bin:$PATH"
